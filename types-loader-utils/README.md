# Installation
> `npm install --save @types/loader-utils`

# Summary
This package contains type definitions for loader-utils (https://github.com/webpack/loader-utils#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/loader-utils.

### Additional Details
 * Last updated: Tue, 06 Jul 2021 22:02:43 GMT
 * Dependencies: [@types/webpack](https://npmjs.com/package/@types/webpack), [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Gyusun Yeom](https://github.com/Perlmint), [Totooria Hyperion](https://github.com/TotooriaHyperion), [Piotr Błażejewicz](https://github.com/peterblazejewicz), and [Jesse Katsumata](https://github.com/Naturalclar).
